package jonss.com.github.githubviewer.screens.screenUtils;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by joao on 27/04/17.
 */

public abstract class NeverEndingScrollListener extends RecyclerView.OnScrollListener {

    private int visibleItemCount, firstVisibleItem, totalItemCount;
    private LinearLayoutManager layoutManager;
    private boolean loading = true;
    private int previousTotal = 0;
    private int currentPage = 1;
    private int visibleThreshold = 6;


    public NeverEndingScrollListener(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = layoutManager.getItemCount();
        firstVisibleItem = layoutManager.findFirstVisibleItemPosition();

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
            }
        }

        if (!loading && (totalItemCount - previousTotal)
                <= (firstVisibleItem + visibleThreshold)) {
            currentPage++;
            onLoadMore(currentPage);
            loading = true;
        }

    }

    public abstract void onLoadMore(int currentPage);

}
