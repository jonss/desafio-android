package jonss.com.github.githubviewer.screens.screenUtils;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;

import jonss.com.github.githubviewer.R;
import jonss.com.github.githubviewer.screens.githubRepositories.RepositoriesListActivity;
import jonss.com.github.githubviewer.screens.settings.SettingsActivity;

import static android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences;

/**
 * Created by joao on 02/05/17.
 */
public class RepositoriesListActivityHelper {

    public static void callDialog(final RepositoriesListActivity activity) {
        new AlertDialog.Builder(activity)
                .setTitle(activity.getString(R.string.app_name))
                .setMessage(activity.getString(R.string.dialog_message))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        activity.startActivity(new Intent(activity, SettingsActivity.class));
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public static String getLanguage(RepositoriesListActivity activity) {
        SharedPreferences sharedPreferences = getDefaultSharedPreferences(activity);
        return sharedPreferences.getString(activity.getString(R.string.pref_language_key),
                activity.getString(R.string.default_language));
    }
}
