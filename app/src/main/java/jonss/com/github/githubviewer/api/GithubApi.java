package jonss.com.github.githubviewer.api;

import java.util.List;

import jonss.com.github.githubviewer.models.pulls.GithubPull;
import jonss.com.github.githubviewer.models.repositories.GithubRepositories;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by joao on 27/04/17.
 */

public interface GithubApi {

    @GET("search/repositories?sort=stars")
    Call<GithubRepositories> getGithubRepos(@Query("page") int page,
                                            @Query("q") String language);

    @GET("repos/{owner}/{project}/pulls")
    Call<List<GithubPull>> getGithubPullRequests(@Path("owner") String owner,
                                                 @Path("project") String project);

}
