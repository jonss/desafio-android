package jonss.com.github.githubviewer.screens.settings;

import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;

import jonss.com.github.githubviewer.R;

/**
 * Created by joao on 01/05/17.
 */

public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.preferences);
    }
}
