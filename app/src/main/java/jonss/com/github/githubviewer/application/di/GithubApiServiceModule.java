package jonss.com.github.githubviewer.application.di;

import dagger.Module;
import dagger.Provides;
import jonss.com.github.githubviewer.api.GithubApi;
import jonss.com.github.githubviewer.utils.Constants;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by joao on 27/04/17.
 */

@Module
public class GithubApiServiceModule {

    @Provides
    Retrofit providesRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(Constants.GITHUB_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    public GithubApi providesGithubService(Retrofit retrofit) {
        return retrofit.create(GithubApi.class);
    }

}
