package jonss.com.github.githubviewer.application.di;

import dagger.Component;
import jonss.com.github.githubviewer.screens.githubRepositories.RepositoriesListActivity;
import jonss.com.github.githubviewer.screens.repositoryPulls.PullsActivity;

/**
 * Created by joao on 27/04/17.
 */

@Component(modules = {GithubApiServiceModule.class})
public interface GithubApplicationComponent {

    void inject(RepositoriesListActivity repositoriesListActivity);
    void inject(PullsActivity pullsActivity);
}
