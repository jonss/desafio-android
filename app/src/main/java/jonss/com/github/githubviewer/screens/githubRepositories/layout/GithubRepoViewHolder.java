package jonss.com.github.githubviewer.screens.githubRepositories.layout;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jonss.com.github.githubviewer.R;
import jonss.com.github.githubviewer.models.repositories.GithubRepository;
import jonss.com.github.githubviewer.screens.repositoryPulls.PullsActivity;

import static jonss.com.github.githubviewer.utils.Constants.REPOSITORY;
import static jonss.com.github.githubviewer.utils.StringUtils.dealWithNumber;

/**
 * Created by joao on 27/04/17.
 */
public class GithubRepoViewHolder extends RecyclerView.ViewHolder {

    private final Context context;

    @BindView(R.id.github_repo_name_tv)
    TextView githubRepoNameTextView;
    @BindView(R.id.github_repo_stars_tv)
    TextView githubStarsTextView;
    @BindView(R.id.github_repo_forks_tv)
    TextView githubForksTextView;
    @BindView(R.id.github_repo_description_tv)
    TextView githubDescriptionTextView;
    @BindView(R.id.github_repo_user_name_tv)
    TextView githubOwnerUserNameTextView;
    @BindView(R.id.github_repo_user_avatar_iv)
    ImageView githubOwnerAvatarImageView;

    private GithubRepository githubRepository;

    public GithubRepoViewHolder(View view) {
        super(view);
        context = view.getContext();
        ButterKnife.bind(this, view);
    }

    public void setGithubRepo(GithubRepository repo) {
        githubRepository = repo;
        this.githubRepoNameTextView.setText(repo.getName());
        this.githubDescriptionTextView.setText(repo.getDescription());
        this.githubStarsTextView.setText(dealWithNumber(repo.getStars()));
        this.githubForksTextView.setText(dealWithNumber(repo.getForks()));
        this.githubOwnerUserNameTextView.setText(repo.getOwnerUserName());
        this.githubDescriptionTextView.setText(repo.getDescription());
        Glide.with(context).load(repo.getOwnerAvatarUrl()).into(githubOwnerAvatarImageView);
    }

    @OnClick(R.id.github_repo_card_view)
    public void sendToPullActivity() {
        Intent pullsIntent = new Intent(context, PullsActivity.class);
        pullsIntent.putExtra(REPOSITORY, githubRepository);
        context.startActivity(pullsIntent);
    }

}
