package jonss.com.github.githubviewer.models.pulls;

import com.google.gson.annotations.SerializedName;

/**
 * Created by joao on 29/04/17.
 */

public class GithubPull {

    private GithubUser user;
    private String title;
    private String body;
    @SerializedName("html_url")
    private String htmlUrl;

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public String getUsername() {
        return user.getUsername();
    }

    public String getUserAvatarUrl() {
        return user.getAvatarUrl();
    }

}
