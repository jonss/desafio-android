package jonss.com.github.githubviewer.screens.settings;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import jonss.com.github.githubviewer.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }
}
