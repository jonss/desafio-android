package jonss.com.github.githubviewer.screens.repositoryPulls;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import jonss.com.github.githubviewer.R;
import jonss.com.github.githubviewer.api.GithubApi;
import jonss.com.github.githubviewer.application.GithubApplication;
import jonss.com.github.githubviewer.models.pulls.GithubPull;
import jonss.com.github.githubviewer.models.repositories.GithubRepository;
import jonss.com.github.githubviewer.screens.repositoryPulls.layout.GithubPullRequestAdapter;
import jonss.com.github.githubviewer.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PullsActivity extends AppCompatActivity {

    private static final String TAG = PullsActivity.class.getSimpleName();
    @Inject
    GithubApi githubApi;

    @BindView(R.id.github_pulls_message)
    TextView githubPullsMessageTextView;
    @BindView(R.id.github_repository_pulls_list_rv)
    RecyclerView githubRepositoryPullsRecyclerView;

    @BindView(R.id.loader_pb)
    ProgressBar loaderProgressBar;

    private GithubPullRequestAdapter githubPullRequestAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pulls);

        ButterKnife.bind(this);

        GithubApplication.getGithubApplicationComponent().inject(this);
        githubRepositoryPullsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        githubPullRequestAdapter = new GithubPullRequestAdapter();
        githubRepositoryPullsRecyclerView.setAdapter(githubPullRequestAdapter);

        GithubRepository repository = getIntent().getParcelableExtra(Constants.REPOSITORY);

        if (repository != null) {
            callApi(repository.getOwnerUserName(), repository.getName());
        }
    }

    public void callApi(String owner, String project) {
        githubApi.getGithubPullRequests(owner, project).enqueue(new Callback<List<GithubPull>>() {
            @Override
            public void onResponse(Call<List<GithubPull>> call, Response<List<GithubPull>> response) {
                githubPullRequestAdapter.setGithubPullList(response.body());
                loaderProgressBar.setVisibility(View.GONE);
                setMessageIfNecessary(response.body().size());
            }

            @Override
            public void onFailure(Call<List<GithubPull>> call, Throwable t) {
                Log.d(TAG, t.getMessage());
            }
        });
    }

    private void setMessageIfNecessary(int size) {
        if (size == 0) {
            githubPullsMessageTextView.setVisibility(View.VISIBLE);
        }
    }

}
