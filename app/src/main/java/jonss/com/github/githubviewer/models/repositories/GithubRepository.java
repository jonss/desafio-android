package jonss.com.github.githubviewer.models.repositories;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by joao on 27/04/17.
 */
public class GithubRepository implements Parcelable {

    @SerializedName("name")
    private String name;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("description")
    private String description;
    @SerializedName("forks_count")
    private Integer forks;
    @SerializedName("stargazers_count")
    private Integer stars;
    @SerializedName("owner")
    private Owner owner;

    protected GithubRepository(Parcel in) {
        owner = in.readParcelable(Owner.class.getClassLoader());
        name = in.readString();
        fullName = in.readString();
        description = in.readString();
        forks = in.readInt();
        stars = in.readInt();
    }

    public static final Creator<GithubRepository> CREATOR = new Creator<GithubRepository>() {
        @Override
        public GithubRepository createFromParcel(Parcel in) {
            return new GithubRepository(in);
        }

        @Override
        public GithubRepository[] newArray(int size) {
            return new GithubRepository[size];
        }
    };

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public String getDescription() {
        return description;
    }

    public Integer getForks() {
        return forks;
    }

    public Integer getStars() {
        return stars;
    }

    public String getOwnerAvatarUrl() {
        return owner.getAvatarUrl();
    }

    public String getOwnerUserName() {
        return owner.getUserName();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(owner, i);
        parcel.writeString(name);
    }
}
