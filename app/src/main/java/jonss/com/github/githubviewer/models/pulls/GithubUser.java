package jonss.com.github.githubviewer.models.pulls;

import com.google.gson.annotations.SerializedName;

/**
 * Created by joao on 29/04/17.
 */
public class GithubUser {

    @SerializedName("login")
    private String username;
    @SerializedName("avatar_url")
    private String avatarUrl;

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getUsername() {
        return username;
    }
}
