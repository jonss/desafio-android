package jonss.com.github.githubviewer.screens.repositoryPulls.layout;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import jonss.com.github.githubviewer.R;
import jonss.com.github.githubviewer.models.pulls.GithubPull;

/**
 * Created by joao on 30/04/17.
 */
public class GithubPullRequestAdapter extends RecyclerView.Adapter<GithubPullRequestViewHolder> {

    private List<GithubPull> githubPullList = new ArrayList<>();

    @Override
    public GithubPullRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.github_pull_item, parent, false);
        return new GithubPullRequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GithubPullRequestViewHolder holder, int position) {
        holder.setPullRequestInfo(githubPullList.get(position));
    }

    @Override
    public int getItemCount() {
        return githubPullList == null || githubPullList.isEmpty()? 0: githubPullList.size();
    }

    public void setGithubPullList(List<GithubPull> body) {
        githubPullList.addAll(body);
        notifyDataSetChanged();
    }
}
