package jonss.com.github.githubviewer.screens.githubRepositories;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import jonss.com.github.githubviewer.R;
import jonss.com.github.githubviewer.api.GithubApi;
import jonss.com.github.githubviewer.application.GithubApplication;
import jonss.com.github.githubviewer.models.repositories.GithubRepositories;
import jonss.com.github.githubviewer.models.repositories.GithubRepository;
import jonss.com.github.githubviewer.screens.githubRepositories.layout.GithubRepositoryAdapter;
import jonss.com.github.githubviewer.screens.screenUtils.NeverEndingScrollListener;
import jonss.com.github.githubviewer.screens.settings.SettingsActivity;
import jonss.com.github.githubviewer.utils.Constants;
import jonss.com.github.githubviewer.utils.StringUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static jonss.com.github.githubviewer.screens.screenUtils.RepositoriesListActivityHelper.callDialog;
import static jonss.com.github.githubviewer.screens.screenUtils.RepositoriesListActivityHelper.getLanguage;
import static jonss.com.github.githubviewer.utils.Constants.GITHUB_REPOSITORIES_KEY;

public class RepositoriesListActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = RepositoriesListActivity.class.getSimpleName();

    @Inject
    GithubApi githubApi;

    @BindView(R.id.github_repo_list_rv)
    RecyclerView githubRepositoriesListRecyclerView;
    private GithubRepositoryAdapter githubRepositoryAdapter;

    private static final int FIRST_PAGE = 1;
    private List<GithubRepository> githubRepositories = new ArrayList<>();
    private boolean PREFERENCES_HAVE_BEEN_UPDATED = false;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTheme(R.style.AppTheme);
        ButterKnife.bind(this);

        GithubApplication.getGithubApplicationComponent().inject(this);

        layoutManager = new LinearLayoutManager(this);
        githubRepositoryAdapter = new GithubRepositoryAdapter();
        githubRepositoriesListRecyclerView.setLayoutManager(layoutManager);
        githubRepositoriesListRecyclerView.setAdapter(githubRepositoryAdapter);

        if (savedInstanceState != null) {
            fillAdapter(savedInstanceState);
        } else {
            fetchData(layoutManager, StringUtils.formatLanguage(getLanguage(this)));
        }

        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
    }

    private void fillAdapter(Bundle savedInstanceState) {
        List<GithubRepository> savedRepositoriesList = savedInstanceState
                .getParcelableArrayList(GITHUB_REPOSITORIES_KEY);
        githubRepositoryAdapter.setGithubRepoData(savedRepositoriesList);
    }

    private void fetchData(final LinearLayoutManager layoutManager, final String language) {
        callApi(FIRST_PAGE, language);
        githubRepositoriesListRecyclerView.addOnScrollListener(new NeverEndingScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                callApi(currentPage, language);
            }
        });
    }

    private void callApi(int currentPage, String language) {
        githubApi.getGithubRepos(currentPage, language).enqueue(new Callback<GithubRepositories>() {
            @Override
            public void onResponse(Call<GithubRepositories> call, Response<GithubRepositories> response) {
                if (response.isSuccessful()) {
                    githubRepositories.addAll(response.body().getGithubRepositories());
                    githubRepositoryAdapter.setGithubRepoData(response.body().getGithubRepositories());
                } else {
                    if (response.code() == Constants.UNPROCESSABLE_ENTITY) {
                        callDialog(RepositoriesListActivity.this);
                    }
                }
            }

            @Override
            public void onFailure(Call<GithubRepositories> call, Throwable t) {
                Log.d(TAG, t.getMessage());
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(GITHUB_REPOSITORIES_KEY,
                (ArrayList<? extends Parcelable>) githubRepositories);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        List<GithubRepository> githubRepositories = savedInstanceState
                .getParcelableArrayList(GITHUB_REPOSITORIES_KEY);
        this.githubRepositories.addAll(githubRepositories);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.repositories_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings_action:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        PREFERENCES_HAVE_BEEN_UPDATED = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (PREFERENCES_HAVE_BEEN_UPDATED) {
            fetchData(layoutManager, StringUtils.formatLanguage(getLanguage(this)));
            PREFERENCES_HAVE_BEEN_UPDATED = false;
        }
    }
}

