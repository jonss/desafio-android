package jonss.com.github.githubviewer.application;

import android.app.Application;


import jonss.com.github.githubviewer.application.di.DaggerGithubApplicationComponent;
import jonss.com.github.githubviewer.application.di.GithubApiServiceModule;
import jonss.com.github.githubviewer.application.di.GithubApplicationComponent;

/**
 * Created by joao on 27/04/17.
 */

public class GithubApplication extends Application {

    private static GithubApplicationComponent githubApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        githubApplicationComponent = DaggerGithubApplicationComponent.builder()
                .githubApiServiceModule(new GithubApiServiceModule())
                .build();
    }

    public static GithubApplicationComponent getGithubApplicationComponent() {
        return githubApplicationComponent;
    }

}
