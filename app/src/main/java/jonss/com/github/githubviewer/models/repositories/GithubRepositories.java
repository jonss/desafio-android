package jonss.com.github.githubviewer.models.repositories;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by joao on 27/04/17.
 */
public class GithubRepositories {

    @SerializedName("items")
    private List<GithubRepository> githubRepositories;

    public List<GithubRepository> getGithubRepositories() {
        return githubRepositories;
    }

}
