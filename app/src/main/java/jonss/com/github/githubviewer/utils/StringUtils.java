package jonss.com.github.githubviewer.utils;

/**
 * Created by joao on 01/05/17.
 */

public class StringUtils {

    public static String dealWithNumber(Integer value) {
        if(value > 999) {
            return String.format("%.1fK", value / 1000.0);
        }
        return String.valueOf(value);
    }

    public static String formatLanguage(String language) {
        return String.format("language:%s", language);
    }
}
