package jonss.com.github.githubviewer.screens.repositoryPulls.layout;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jonss.com.github.githubviewer.R;
import jonss.com.github.githubviewer.models.pulls.GithubPull;

/**
 * Created by joao on 30/04/17.
 */
public class GithubPullRequestViewHolder extends RecyclerView.ViewHolder {

    private final Context context;
    private GithubPull githubPull;
    @BindView(R.id.github_pull_request_title_tv)
    TextView githubTitleTextView;
    @BindView(R.id.github_pull_request_body_tv)
    TextView githubPullBodyTextView;
    @BindView(R.id.github_pull_request_user_login_tv)
    TextView githubPullUserLoginTextView;
    @BindView(R.id.github_pull_request_avatar_iv)
    ImageView githubPullAvatarUrlImageView;

    public GithubPullRequestViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        context = itemView.getContext();
    }

    public void setPullRequestInfo(GithubPull githubPull) {
        this.githubPull = githubPull;
        githubTitleTextView.setText(githubPull.getTitle());
        if (!githubPull.getBody().isEmpty()) {
            githubPullBodyTextView.setText(githubPull.getBody());
        }
        githubPullUserLoginTextView.setText(githubPull.getUsername());
        Glide.with(context)
                .load(githubPull.getUserAvatarUrl())
                .into(githubPullAvatarUrlImageView);
    }

    @OnClick(R.id.github_pull_request_card_view)
    public void openPullRequest() {
        Uri githubPullUri = Uri.parse(githubPull.getHtmlUrl());
        Intent intent = new Intent(Intent.ACTION_VIEW, githubPullUri);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

}
