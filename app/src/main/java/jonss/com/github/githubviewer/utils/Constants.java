package jonss.com.github.githubviewer.utils;

/**
 * Created by joao on 27/04/17.
 */

public class Constants {

    public static final String GITHUB_API = "https://api.github.com";
    public static final String GITHUB_REPOSITORIES_KEY = "GITHUB_REPOSITORIES_KEY";
    public static final int UNPROCESSABLE_ENTITY = 422;
    public static String REPOSITORY = "REPOSITORY";
}
