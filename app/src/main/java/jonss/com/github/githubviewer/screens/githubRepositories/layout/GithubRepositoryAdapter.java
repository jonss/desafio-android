package jonss.com.github.githubviewer.screens.githubRepositories.layout;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import jonss.com.github.githubviewer.R;
import jonss.com.github.githubviewer.models.repositories.GithubRepository;

/**
 * Created by joao on 29/04/17.
 */
public class GithubRepositoryAdapter extends RecyclerView.Adapter<GithubRepoViewHolder> {

    private List<GithubRepository> githubRepositories = new ArrayList<>();

    @Override
    public GithubRepoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.github_repo_item, parent, false);
        return new GithubRepoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GithubRepoViewHolder holder, int position) {
        holder.setGithubRepo(githubRepositories.get(position));
    }

    @Override
    public int getItemCount() {
        return githubRepositories == null || githubRepositories.isEmpty() ?
                0 : githubRepositories.size();
    }

    public void setGithubRepoData(List<GithubRepository> githubRepositories) {
        this.githubRepositories.addAll(githubRepositories);
        notifyDataSetChanged();
    }

}