package jonss.com.github.githubviewer.utils;

import junit.framework.Assert;

import org.junit.Test;

/**
 * Created by joao on 01/05/17.
 */
public class StringUtilsTest {


    @Test
    public void shouldReturn4comma2KForNumberAboveIs4200() {
        Assert.assertEquals("4,2K", StringUtils.dealWithNumber(4232));
    }

    @Test
    public void shouldReturnTheRealNumberWhenItIsLowerThan1000() {
        Assert.assertEquals("999", StringUtils.dealWithNumber(999));
    }

}
